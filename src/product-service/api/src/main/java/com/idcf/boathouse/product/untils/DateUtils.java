package com.idcf.boathouse.product.untils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * date:2020/3/17 18:16
 * author:xiaokunliu
 * desc: business desc etc.
 */
public final class DateUtils {

    public static String formatTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }
}
